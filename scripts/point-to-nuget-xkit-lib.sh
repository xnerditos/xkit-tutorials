#!/bin/bash

currFolder="$(pwd)"  # Absolute path to this script, e.g. /home/user/bin/foo.sh
scriptPath=$(readlink -f "$0")  # Absolute path this script is in
scriptDir=$(dirname "$scriptPath")

for projFile in $(find $scriptDir/.. -type f -name "*.csproj"); do
  echo "switching $projFile to nuget reference"
  "$scriptDir/point-csproj-to-nuget-lib.sh" "$projFile"
done

"$scriptDir/clean-build-files.sh"
