#!/bin/bash

csproj=$1

if [ -z "$csproj" ]; then
  echo "Please specify project file to target"
  exit
fi

sed -i "s+.*XKit.Lib.Host.csproj\".*+    <PackageReference Include=\"XKit.Lib.Host\" Version=\"*\"/>+g" "$csproj"
sed -i "s+.*XKit.Lib.Host.Helpers.csproj\".*+    <PackageReference Include=\"XKit.Lib.Host.Helpers\" Version=\"*\"/>+g" "$csproj"
sed -i "s+.*XKit.Lib.Host.Protocols.Http.Mvc.csproj\".*+    <PackageReference Include=\"XKit.Lib.Host.Protocols.Http.Mvc\" Version=\"*\"/>+g" "$csproj"
sed -i "s+.*XKit.Lib.LocalLog.csproj\".*+    <PackageReference Include=\"XKit.Lib.LocalLog\" Version=\"*\"/>+g" "$csproj"
sed -i "s+.*XKit.Lib.Common.csproj\".*+    <PackageReference Include=\"XKit.Lib.Common\" Version=\"*\"/>+g" "$csproj"
sed -i "s+.*XKit.Lib.Common.MetaServices.csproj\".*+    <PackageReference Include=\"XKit.Lib.Common.MetaServices\" Version=\"*\"/>+g" "$csproj"
sed -i "s+.*XKit.Lib.Connector.csproj\".*+    <PackageReference Include=\"XKit.Lib.Connector\" Version=\"*\"/>+g" "$csproj"
sed -i "s+.*XKit.Lib.Connector.Protocols.Direct.csproj\".*+    <PackageReference Include=\"XKit.Lib.Connector.Protocols.Direct\" Version=\"*\"/>+g" "$csproj"
sed -i "s+.*XKit.Lib.Connector.Protocols.Http.csproj\".*+    <PackageReference Include=\"XKit.Lib.Connector.Protocols.Http\" Version=\"*\"/>+g" "$csproj"
sed -i "s+.*XKit.Lib.Testing.csproj\".*+    <PackageReference Include=\"XKit.Lib.Testing\" Version=\"*\"/>+g" "$csproj"
#sed -i "s+.*XKit.Lib.Host.Sync.csproj\".*+    <PackageReference Include=\"XKit.Lib.Host.Sync\" Version=\"*\"/>+g" "$csproj"
sed -i "s+.*XKit.Lib.Storage.csproj\".*+    <PackageReference Include=\"XKit.Lib.LocalStorage\" Version=\"*\"/>+g" "$csproj"
