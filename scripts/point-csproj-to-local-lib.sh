#!/bin/bash

csproj=$1
localpathroot=$2

if [ -z "$csproj" ]; then
  echo "Please specify project file to target"
  exit
fi
if [ -z "$localpathroot" ]; then
  echo "Please specify the local path root"
  exit
fi

sed -i "s+.*Include=\"XKit.Lib.Host\".*+    <ProjectReference Include=\"$localpathroot/XKit.Lib.Host/XKit.Lib.Host.csproj\" />+g" "$csproj"
sed -i "s+.*Include=\"XKit.Lib.Host.Helpers\".*+    <ProjectReference Include=\"$localpathroot/XKit.Lib.Host.Helpers/XKit.Lib.Host.Helpers.csproj\" />+g" "$csproj"
sed -i "s+.*Include=\"XKit.Lib.Host.Protocols.Http.Mvc\".*+    <ProjectReference Include=\"$localpathroot/XKit.Lib.Host.Protocols.Http.Mvc/XKit.Lib.Host.Protocols.Http.Mvc.csproj\" />+g" "$csproj"
sed -i "s+.*Include=\"XKit.Lib.LocalLog\".*+    <ProjectReference Include=\"$localpathroot/XKit.Lib.LocalLog/XKit.Lib.LocalLog.csproj\" />+g" "$csproj"
sed -i "s+.*Include=\"XKit.Lib.Common\".*+    <ProjectReference Include=\"$localpathroot/XKit.Lib.Common/XKit.Lib.Common.csproj\" />+g" "$csproj"
sed -i "s+.*Include=\"XKit.Lib.Common.MetaServices\".*+    <ProjectReference Include=\"$localpathroot/XKit.Lib.Common.MetaServices/XKit.Lib.Common.MetaServices.csproj\" />+g" "$csproj"
sed -i "s+.*Include=\"XKit.Lib.Connector\".*+    <ProjectReference Include=\"$localpathroot/XKit.Lib.Connector/XKit.Lib.Connector.csproj\" />+g" "$csproj"
sed -i "s+.*Include=\"XKit.Lib.Connector.Protocols.Direct\".*+    <ProjectReference Include=\"$localpathroot/XKit.Lib.Connector.Protocols.Direct/XKit.Lib.Connector.Protocols.Direct.csproj\" />+g" "$csproj"
sed -i "s+.*Include=\"XKit.Lib.Connector.Protocols.Http\".*+    <ProjectReference Include=\"$localpathroot/XKit.Lib.Connector.Protocols.Http/XKit.Lib.Connector.Protocols.Http.csproj\" />+g" "$csproj"
sed -i "s+.*Include=\"XKit.Lib.Testing\".*+    <ProjectReference Include=\"$localpathroot/XKit.Lib.Testing/XKit.Lib.Testing.csproj\" />+g" "$csproj"
#sed -i "s+.*Include=\"XKit.Lib.Host.Sync\".*+    <ProjectReference Include=\"$localpathroot/XKit.Lib.Host/XKit.Lib.Host.csproj\" />+g" "$csproj"
sed -i "s+.*Include=\"XKit.Lib.Storage\".*+    <ProjectReference Include=\"$localpathroot/XKit.Lib.LocalStorage/XKit.Lib.LocalStorage.csproj\" />+g" "$csproj"
