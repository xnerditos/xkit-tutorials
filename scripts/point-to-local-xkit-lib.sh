#!/bin/bash

currFolder="$(pwd)"  # Absolute path to this script, e.g. /home/user/bin/foo.sh
scriptPath=$(readlink -f "$0")  # Absolute path this script is in
scriptDir=$(dirname "$scriptPath")

localpathroot=$1
if [ -z "$localpathroot" ]; then
  echo "Please specify the local path root"
  exit
fi

for projFile in $(find $scriptDir/.. -type f -name "*.csproj"); do
  echo "switching $projFile to local reference"
  "$scriptDir/point-csproj-to-nuget-lib.sh" "$projFile"
  "$scriptDir/point-csproj-to-local-lib.sh" "$projFile" "$localpathroot"
done

"$scriptDir/clean-build-files.sh"

