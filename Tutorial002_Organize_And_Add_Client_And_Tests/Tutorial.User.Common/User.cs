namespace Tutorial.User
{
    public class User
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
    }
}