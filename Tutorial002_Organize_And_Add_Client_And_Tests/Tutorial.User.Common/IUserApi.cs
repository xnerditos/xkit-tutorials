using System.Threading.Tasks;
using XKit.Lib.Common.Fabric;
using XKit.Lib.Common.Host;

namespace Tutorial.User
{
    public interface IUserApi : IServiceApi
    {
        Task<ServiceApiResult> UpsertUser(User request);
        Task<ServiceApiResult> DeleteUser(User request);
        Task<ServiceApiResult<User>> GetUser(User request);
    }
}