using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using XKit.Lib.Common.Registration;
using XKit.Lib.Host;

namespace Tutorial.User
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = HostEnvironmentHelper.CreateInitHost(hostAddress: "localhost");
            host.AddCreateManagedService(
                serviceDescriptor: Constants.ServcieDescriptor,
                typeof(ApiOperation)
            );
            host.StartHost(
                initialRegistryHostAddresses: null,
                monitor: null
            );
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
