# Welcome

Welcome to the XerviceKit tutorials.  

## Instructions

This repository has tutorials to help you understand how to use XerviceKit.  Use the table of contents below to visit the tutorial of your choice.  The README in each folder explains the tutorial and takes you through it. 

Each tutorial starts with a base of code, usually from where the last tutorial left off.  The tutorial will focus on helping you understand a particular point.  Some will point you to a presentation that gives you some background on the particular point.   They will guide you through making some changes and explain what is happening. Finally, most tutorials will give you ask you to make a further modification to the code based on what you learned. 

The tutorials are designed to be bite-sized chunks.  The intention is that each one take you no more than an hour and in many cases, much less.  

If you are learning from scratch, we recommend starting at the beginning and proceeding until you are are comfortable.  If you want information about a particular subject, you can jump to the tutorial that covers it.

## Contents

These are the tutorials currently available.  More are added as time goes on. 

[Tutorial 1:  Create a basic service](https://bitbucket.org/xnerditos/xkit-tutorials/src/master/Tutorial001_Create_A_Basic_Service)

* Using the paradigm of a simple  "CRUD" service for managing Users, this tutorial starts you off with creating the basic project using XerviceKit to create a microservice with an API.  

[Tutorial 2:  Reorganize and add client and tests](https://bitbucket.org/xnerditos/xkit-tutorials/src/master/Tutorial002_Organize_And_Add_Client_And_Tests/)

* Using the paradigm of a simple  "CRUD" service for managing Users, this tutorial starts you off with creating the basic project using XerviceKit to create a microservice with an API.  
