using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using XKit.Lib.Common.Registration;
using XKit.Lib.Connector.Protocols.Direct;
using XKit.Lib.Connector.Protocols.Http;
using XKit.Lib.Host;

namespace Tutorial.User
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = HostEnvironmentHelper.CreateInitHost(hostAddress: "localhost");
            host.AddCreateManagedService(
                serviceDescriptor: new Descriptor {
                    Collection = "Tutorial",
                    Name = "User",
                    Version = 1
                },
                typeof(ApiOperation)
            );
            host.StartHost(
                initialRegistryHostAddresses: null,
                monitor: null
            );
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
